def vowel_swapper(string):
    # ==============
    # Your code here
    import re
    string = re.sub(r'^(.*?([aA].*?))[aA]', r'\g<1>4', string)
    string = re.sub(r'^(.*?([eE].*?))[eE]', r'\g<1>3', string)
    string = re.sub(r'^(.*?([iI].*?))[iI]', r'\1!', string)
    string = re.sub(r'^(.*?([oO].*?))([oO])', lambda x: x.group(1)+"ooo" if x.group(3)=="o" else x.group(1)+"000", string)
    string = re.sub(r'^(.*?([uU].*?))[uU]', r'\1|_|', string)
    return string
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console

